#!/usr/bin/env bash
########################
#created by Oleg
#porpuse study
#date 4.3.21
########################

_installer=yum
_pkgs=(epel-release nginx gunicorn)
_project="/opt"

install_tools(){

	for _pkg in $_pkgs
	  do
		 $_installer install -y $_pkg
	 done
} 
	 

get_app_from_git(){
	git clone https://gitlab.com/Alejka/nginx-with-flask.git #path for app

}

init_up(){
	
	cd $_project/app
	pip install -r requirements.txt
}

conf_nginx(){
	echo "
	server {
		listen 80;
		server_name 10.0.2.15;
		location / {
			     proxy_pass http://unix:/opt/app/app.sock;
		     	     proxy_set_header Host $http host;
			     proxy_set_header X-Real -IP $remote_addr;
			     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			     proxy_set_header X-Forwarded-Proto $scheme;
	}
 }" > /etc/nginx/conf.d/flask.conf


 if [[ $(nginx -t >/dev/null; echo $?) == 0 ]];then
	 systemctl enable --now nginx
 else
	 echo "There is an ERORR with nginx config"
 fi
}

systemd_conf(){
echo "	
[Unit]
Description=Flask Daemon
After=network.target

[Service]
User=nginx
Group=nginx
WorkingDirectory=/opt/app
ExecStart=/usr/local/bin/gunicorn/ --workers 3 --bind unix:/opt/app/app.sock app:app ;

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/flask.service
systemctl daemon-reload
sleep 2
systemctl enable --now flask
}

main(){
if [[ $EUID!= 0 ]]
	echo "Cannot provision server, your are not a root user ."
else
	install_tools
	conf_nginx
	get_app_from_git
	init_app
	systemd_conf
fi
}

main
